# HACKERRANK.COM #
## /katonatomi ##
Tamas Katona Solutions for Training Purposes using Java

### PROBLEM SOLVING /JAVA ###
* AngryProfessor
* AppleAndOrange
* Arrays-DS
* AVeryBigSum
* BeautifulDaysattheMovies
* BetweenTwoSets
* BillDivision
* BirthdayCakeCandles
* BreakingTheRecords
* CatsAndMouse
* ClimbingTheLeaderboard
* CompareTheTriplets
* CountingValleys
* DayOfTheProgrammer
* DesignerPdfViewer
* DiagonalDifference
* DivisibleSumPairs
* DrawingBook
* DynamicArray
* ElectronicsShop
* ExtraLongFactorials
* FindDigits
* FormingAMagicSquare
* GradingStudents
* JavaBigDecimal
* JavaDateandTime
* JavaRegex
* JavaRegex2-DuplicateWords
* JavaStaticInitializerBlock
* JavaStringReverse
* MigratoryBirds
* MinMaxSum
* NumberLineJumps
* PickingNumbers
* PlusMinus
* PrimeChecker
* SalesByMatch
* SimpleArraySum
* Staircase
* Sub-arrayDivision
* TagContentExtractor
* TheHurdleRace
* TimeConversion
* UtopianTree
* ValidUsernameRegularExpression
* ViralAdvertising

### PROBLEM SOLVING /CSHARP ###
* LeftRotation
* SparseArrays
* PrintElementsOfLinkedList
* InsertNodeToTailOfLinkedList
* InsertNodeHeadOfLinkedList
* InsertNodeAtPositionOfLinkedList
* DeleteANode
