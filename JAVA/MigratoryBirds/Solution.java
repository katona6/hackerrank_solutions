import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    // Complete the migratoryBirds function below.
    static int migratoryBirds(List<Integer> arr) {
        
        //find Max Value in ARR
        int arrMaxValue = 0;
        for (int currentItem : arr){
            if (currentItem > arrMaxValue) arrMaxValue = currentItem;
        }
        
        //build 2nd array (itemFreqs) with each item freqency
        int[] itemFreqs = new int[arrMaxValue];
        
        for (int currentItem : arr){
            itemFreqs[currentItem - 1]++;
        }
        
        //find maxFreqCounter
        int maxFreqCounter = 0;
        for (int currentItem : itemFreqs){
            if (currentItem > maxFreqCounter) maxFreqCounter = currentItem;
        }
        
        // where we first found the maxFreqCounter return immediately with index.
        for (int i=0; i < itemFreqs.length; i++){
            if (itemFreqs[i] == maxFreqCounter) {
                return i + 1;
            }
        }
        
        //un-needed
        return 0;
        
    }

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int arrCount = Integer.parseInt(bufferedReader.readLine().trim());

        String[] arrItems = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

        List<Integer> arr = new ArrayList<>();

        for (int i = 0; i < arrCount; i++) {
            int arrItem = Integer.parseInt(arrItems[i]);
            arr.add(arrItem);
        }

        int result = migratoryBirds(arr);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedReader.close();
        bufferedWriter.close();
    }
}
