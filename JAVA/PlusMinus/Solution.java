import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    // Complete the plusMinus function below.
    static void plusMinus(int[] arr) {
        byte element_number = (byte) arr.length;
        byte positive_counter = 0;
        byte zero_counter = 0;
        byte negative_counter = 0;
        
        for (int actual : arr){
            if (actual > 0) positive_counter++;
            if (actual == 0) zero_counter++;
            if (actual < 0) negative_counter++;
        }
        NumberFormat formatter = new DecimalFormat("#0.000000");
        System.out.println(formatter.format((double) positive_counter/element_number));
        System.out.println(formatter.format((double) negative_counter/element_number));
        System.out.println(formatter.format((double) zero_counter/element_number));

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] arr = new int[n];

        String[] arrItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int arrItem = Integer.parseInt(arrItems[i]);
            arr[i] = arrItem;
        }

        plusMinus(arr);

        scanner.close();
    }
}
