import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

class Result {

    /*
     * Complete the 'getTotalX' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts following parameters:
     *  1. INTEGER_ARRAY a
     *  2. INTEGER_ARRAY b
     */

   public static int getTotalX(List<Integer> a, List<Integer> b) {
        int gecede = 0;
        int elceem = a.get(0);
        int result_counter = 0;
        int t = 0;

        for (int i = 0; i < b.size(); i++) {
            gecede = findGecede(b.get(i), gecede);
        }
        for (int i = 0; i < a.size() - 1; i++) {
            elceem = (elceem * a.get(i + 1)) / findGecede(a.get(i + 1), elceem);
        }
        for (int i = 1; i <= gecede && t <= gecede; i++) {
            t = elceem * i;
            if (gecede % (elceem * i) == 0) {
                result_counter++;
            }
        }
        return result_counter;

    }
    
    private static int findGecede(int a, int b) {
        return b == 0 ? a : findGecede(b, a % b);
    }

}

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String[] firstMultipleInput = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

        int n = Integer.parseInt(firstMultipleInput[0]);

        int m = Integer.parseInt(firstMultipleInput[1]);

        String[] arrTemp = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

        List<Integer> arr = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            int arrItem = Integer.parseInt(arrTemp[i]);
            arr.add(arrItem);
        }

        String[] brrTemp = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

        List<Integer> brr = new ArrayList<>();

        for (int i = 0; i < m; i++) {
            int brrItem = Integer.parseInt(brrTemp[i]);
            brr.add(brrItem);
        }

        int total = Result.getTotalX(arr, brr);

        bufferedWriter.write(String.valueOf(total));
        bufferedWriter.newLine();

        bufferedReader.close();
        bufferedWriter.close();
    }
}

