import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    // Complete the miniMaxSum function below.
    static void miniMaxSum(int[] arr) {
        
        byte exceptional_element = 0;
        float[] temporarySum = new float[5];
        float minSum = 1000000.0f;
        float maxSum = 0.0f;
        
        do{
            
            for(byte element = 0; element < arr.length; element++){
                if(element != exceptional_element){
                    temporarySum[exceptional_element] += arr[element];
                }
                //System.out.println(temporarySum[exceptional_element]);
            }
            
            exceptional_element++;
        }while(exceptional_element < 5);
        
        
         //Find MIN
        for(byte element = 0; element < temporarySum.length; element++){
            if(temporarySum[element] < minSum){
                minSum = temporarySum[element];
            }
        }
        System.out.print((int) minSum);
        System.out.print(" ");
        
        //Find MAX
        for(byte element = 0; element < temporarySum.length; element++){
            if(temporarySum[element] > maxSum){
                maxSum = temporarySum[element];
            }
        }
        System.out.print((int) maxSum);
       
        


    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int[] arr = new int[5];

        String[] arrItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < 5; i++) {
            int arrItem = Integer.parseInt(arrItems[i]);
            arr[i] = arrItem;
        }

        miniMaxSum(arr);

        scanner.close();
    }
}
