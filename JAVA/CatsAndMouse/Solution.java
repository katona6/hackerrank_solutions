import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    // Complete the catAndMouse function below.
    static String catAndMouse(int x, int y, int z) {
        boolean catA_NeedsPositiveMove = false;
        boolean catB_NeedsPositiveMove = false;
        int catA_DistanceFromMouse = 0;
        int catB_DistanceFromMouse = 0;
        
        if (x < z)
            catA_NeedsPositiveMove = true;
        if (y < z)
            catB_NeedsPositiveMove = true;
        
        if(catA_NeedsPositiveMove){
            catA_DistanceFromMouse = z - x ;
        } else {
            catA_DistanceFromMouse = x - z ;
        }
        if(catB_NeedsPositiveMove){
            catB_DistanceFromMouse = z - y ;
        } else {
            catB_DistanceFromMouse = y - z ;
        }
        
        if (catA_DistanceFromMouse > catB_DistanceFromMouse){
            return "Cat B";
        } else if(catA_DistanceFromMouse < catB_DistanceFromMouse){
            return "Cat A";
        } else {
            return "Mouse C";
        }
        
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int q = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int qItr = 0; qItr < q; qItr++) {
            String[] xyz = scanner.nextLine().split(" ");

            int x = Integer.parseInt(xyz[0]);

            int y = Integer.parseInt(xyz[1]);

            int z = Integer.parseInt(xyz[2]);

            String result = catAndMouse(x, y, z);

            bufferedWriter.write(result);
            bufferedWriter.newLine();
        }

        bufferedWriter.close();

        scanner.close();
    }
}
