import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    // Complete the dayOfProgrammer function below.
    static String dayOfProgrammer(int year) throws Exception{
        
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        int d = 243;
        if (year >= 1700 && year <= 1917) {
            if (year % 4 == 0) {
                d = 244;
            }
        } else if (year >= 1919 && year <= 2700) {
            if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) {
                d = 244;
            }
        } else if (year == 1918) {
            d = 230;
        }
        int r = 256 - d;
        String date = r + "." + 9 + "." + year;
        return sdf.format(sdf.parse(date));


    }

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int year = Integer.parseInt(bufferedReader.readLine().trim());
        try{
            String result = dayOfProgrammer(year);
            bufferedWriter.write(result);
            bufferedWriter.newLine();

            bufferedReader.close();
            bufferedWriter.close();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        
    }
}
