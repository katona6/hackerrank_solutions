package com.hackerrank.problemsolving.ClimbingTheLeaderboard;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

class Result {

	/*
	 * Complete the 'climbingLeaderboard' function below.
	 *
	 * The function is expected to return an INTEGER_ARRAY. The function accepts
	 * following parameters: 1. INTEGER_ARRAY ranked 2. INTEGER_ARRAY player
	 */
	
	public static List<Integer> climbingLeaderboard(List<Integer> ranked, List<Integer> player) {
		//transfer ranked --> scores[]
		int[] leaderBoardPoints = new int[ranked.size()];
		leaderBoardPoints[0] = ranked.get(0);
		int k = 1, duplicateCounter = 0;
		for (int lBP_i = 1; lBP_i < ranked.size(); lBP_i++) {
			int temp = ranked.get(lBP_i);
			if (temp != leaderBoardPoints[k - 1]) {
				leaderBoardPoints[k++] = temp;
			} else {
				duplicateCounter++;
			}
		}
		for (int i = leaderBoardPoints.length - 1; i >= 0 && duplicateCounter > 0; i--) {
			duplicateCounter--;
			leaderBoardPoints[i] = Integer.MIN_VALUE;
		}
		//end of Processing leaderBoardPoints
		List<Integer> resultList = new ArrayList<>();
		
		for (int playerPoints = 0; playerPoints < player.size(); playerPoints++) {
			int tmp = player.get(playerPoints);
			if (tmp > leaderBoardPoints[0]) {
				resultList.add(1);
			} else if (tmp < leaderBoardPoints[leaderBoardPoints.length - 1]) {
				resultList.add(leaderBoardPoints.length + 1);
			} else {
				resultList.add(binarySearch(leaderBoardPoints, tmp) + 1);
			}
		}
		return resultList;
		
	}

	public static List<Integer> climbingLeaderboard_original(List<Integer> ranked, List<Integer> player) {
		List<Integer> resultList = new ArrayList<>();
		SortedSet<Integer> leaderboardPoints = new TreeSet<Integer>();
		// go through player-scores

		for (int currentPlayerScore = 0; currentPlayerScore < player.size(); currentPlayerScore++) {

			ranked.add(player.get(currentPlayerScore));
			Collections.sort(ranked);
//			System.out.println("Beszuras utáni rendezett LeaderBoard :");
//			System.out.println(ranked);

			leaderboardPoints.clear();
			leaderboardPoints.addAll(ranked);

			// convert it to an array
			Integer[] leaderboardPointsArray = leaderboardPoints.toArray(new Integer[leaderboardPoints.size()]);

//	        System.out.println("Without duplicates:");
//			System.out.println(leaderboardPoints);

			resultList.add(leaderboardPointsArray.length
					- Arrays.asList(leaderboardPointsArray).indexOf(player.get(currentPlayerScore)));

		}

		return resultList;

	}
	private static int binarySearch(int[] a, int key) {

		int lo = 0;
		int hi = a.length - 1;

		while (lo <= hi) {
			int mid = lo + (hi - lo) / 2;
			if (a[mid] == key) {
				return mid;
			} else if (a[mid] < key && key < a[mid - 1]) {
				return mid;
			} else if (a[mid] > key && key >= a[mid + 1]) {
				return mid + 1;
			} else if (a[mid] < key) {
				hi = mid - 1;
			} else if (a[mid] > key) {
				lo = mid + 1;
			}
		}
		return -1;
	}

}

public class Solution {
	public static void main(String[] args) throws IOException {
		// BufferedReader bufferedReader = new BufferedReader(new
		// InputStreamReader(System.in));
		// BufferedWriter bufferedWriter = new BufferedWriter(new
		// FileWriter(System.getenv("OUTPUT_PATH")));

//        int rankedCount = Integer.parseInt(bufferedReader.readLine().trim());
		int rankedCount = Integer.parseInt("6");

		// String[] rankedTemp = bufferedReader.readLine().replaceAll("\\s+$",
		// "").split(" ");
		String[] rankedTemp = "100 90 90 80 75 60".replaceAll("\\s+$", "").split(" ");

		List<Integer> ranked = new ArrayList<>();

		for (int i = 0; i < rankedCount; i++) {
			int rankedItem = Integer.parseInt(rankedTemp[i]);
			ranked.add(rankedItem);
		}
		// összeállt a ranked

		// int playerCount = Integer.parseInt(bufferedReader.readLine().trim());
		int playerCount = Integer.parseInt("5");

		// String[] playerTemp = bufferedReader.readLine().replaceAll("\\s+$",
		// "").split(" ");
		String[] playerTemp = "50 65 77 90 102".replaceAll("\\s+$", "").split(" ");

		List<Integer> player = new ArrayList<>();

		for (int i = 0; i < playerCount; i++) {
			int playerItem = Integer.parseInt(playerTemp[i]);
			player.add(playerItem);
		}
		// összeállt a player

		System.out.print("\n\n");
		List<Integer> result = Result.climbingLeaderboard(ranked, player);
//		List<Integer> result = new ArrayList<Integer>();
//		result.add(6);
//		result.add(5);
//		result.add(4);
//		result.add(2);
//		result.add(1);

		for (int i = 0; i < result.size(); i++) {
			System.out.print(String.valueOf(result.get(i)));

			if (i != result.size() - 1) {
				System.out.print("\n");
			}
		}

		// bufferedWriter.newLine();

		// bufferedReader.close();
		// bufferedWriter.close();
	}
}
