﻿using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

namespace ProblemSolving.DeleteANode
{
    class Solution
    {

        class SinglyLinkedListNode
        {
            public int data;
            public SinglyLinkedListNode next;

            public SinglyLinkedListNode(int nodeData)
            {
                this.data = nodeData;
                this.next = null;
            }
        }

        class SinglyLinkedList
        {
            public SinglyLinkedListNode head;
            public SinglyLinkedListNode tail;

            public SinglyLinkedList()
            {
                this.head = null;
                this.tail = null;
            }

            public void InsertNode(int nodeData)
            {
                SinglyLinkedListNode node = new SinglyLinkedListNode(nodeData);

                if (this.head == null)
                {
                    this.head = node;
                }
                else
                {
                    this.tail.next = node;
                }

                this.tail = node;
            }
        }

        static void PrintSinglyLinkedList(SinglyLinkedListNode node, string sep)
        {
            while (node != null)
            {
                Console.Write(node.data);

                node = node.next;

                if (node != null)
                {
                    Console.Write(sep);
                }
            }
        }

        class Result
        {

            /*
             * Complete the 'deleteNode' function below.
             *
             * The function is expected to return an INTEGER_SINGLY_LINKED_LIST.
             * The function accepts following parameters:
             *  1. INTEGER_SINGLY_LINKED_LIST llist
             *  2. INTEGER position
             */

            /*
             * For your reference:
             *
             * SinglyLinkedListNode
             * {
             *     int data;
             *     SinglyLinkedListNode next;
             * }
             *
             */

            public static SinglyLinkedListNode deleteNode(SinglyLinkedListNode llist, int position)
            {
                //save header to --> currentNode
                SinglyLinkedListNode currentNode = llist;
                if (position == 0)
                {
                    return currentNode.next;
                }
                while (--position > 0)
                {
                    currentNode = currentNode.next;
                }
                currentNode.next = currentNode.next.next;

                return llist;
            }

        }

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            SinglyLinkedList llist = new SinglyLinkedList();

            int llistCount = Convert.ToInt32(Console.ReadLine());

            for (int i = 0; i < llistCount; i++)
            {
                int llistItem = Convert.ToInt32(Console.ReadLine());
                llist.InsertNode(llistItem);
            }

            int position = Convert.ToInt32(Console.ReadLine());

            SinglyLinkedListNode llist1 = Result.deleteNode(llist.head, position);

            PrintSinglyLinkedList(llist1, " ");
            Console.WriteLine();

            //textWriter.Flush();
            //textWriter.Close();
        }
    }

}
