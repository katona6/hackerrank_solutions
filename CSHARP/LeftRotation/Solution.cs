﻿using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    /*
     * Complete the 'rotateLeft' function below.
     *
     * The function is expected to return an INTEGER_ARRAY.
     * The function accepts following parameters:
     *  1. INTEGER d
     *  2. INTEGER_ARRAY arr
     */

    public static List<int> RotateLeft(int d, List<int> arr)
    {
        //Create another array to keep original unchanged.
        List<int> resultList = new();
        foreach (var item in arr)
        {
            resultList.Add(item);
        }
        //PROGRESS -> Calculate the shifted positions, and move data once for best performance.
        for (int i = d; i < arr.Count; i++)
        {
            resultList[i - d] = arr[i];
        }

        for (int j = 0; j < d; j++)
        {
            resultList[arr.Count - d + j] = arr[j];
        }

        return resultList;

        /**My original Solution
         
         int iterationCounter = 0;
        while (iterationCounter != d)
        {
            int tempNumber = arr[0];

            for (int i = 0; i < arr.Count - 1; i++)
            {
                arr[i] = arr[i + 1];
            }
            arr[arr.Count - 1] = tempNumber;

            iterationCounter++;
        }
        
        return arr;
         
         */
        
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string[] firstMultipleInput = "5 4".Split(' ');

        int n = Convert.ToInt32(firstMultipleInput[0]);

        int d = Convert.ToInt32(firstMultipleInput[1]);

        List<int> arr = "1 2 3 4 5".TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList();

        List<int> result = Result.RotateLeft(d, arr);

        Console.WriteLine(String.Join(" ", result));

        //textWriter.Flush();
        //textWriter.Close();
    }
}
