﻿using System;

namespace ProblemSolving.InsertNodeAtPositionOfLinkedList
{
    class Solution
    {
        class SinglyLinkedListNode
        {
            public int data;
            public SinglyLinkedListNode next;

            public SinglyLinkedListNode(int nodeData)
            {
                this.data = nodeData;
                this.next = null;
            }
        }

        class SinglyLinkedList
        {
            public SinglyLinkedListNode head;
            public SinglyLinkedListNode tail;

            public SinglyLinkedList()
            {
                this.head = null;
                this.tail = null;
            }

            public static void PrintSinglyLinkedList(SinglyLinkedListNode node, string sep)
            {
                while (node != null)
                {
                    Console.Write(node.data);

                    node = node.next;

                    if (node != null)
                    {
                        Console.Write(sep);
                    }
                }
            }

            public void InsertNode(int data)
            {
                SinglyLinkedListNode newNode = new SinglyLinkedListNode(data);

                if (this.head == null)
                {
                    this.head = newNode;
                }
                else
                {
                    this.tail.next = newNode;
                }

                this.tail = newNode;
            }

            /*
             * Complete the 'insertNodeAtPosition' function below.
             *
             * The function is expected to return an INTEGER_SINGLY_LINKED_LIST.
             * The function accepts following parameters:
             *  1. INTEGER_SINGLY_LINKED_LIST llist
             *  2. INTEGER data
             *  3. INTEGER position
             */

            /*
             * For your reference:
             *
             * SinglyLinkedListNode
             * {
             *     int data;
             *     SinglyLinkedListNode next;
             * }
             *
             */

            public SinglyLinkedListNode InsertNodeAtPosition(SinglyLinkedListNode llist, int data, int position)
            {
                if (llist == null)
                {
                    SinglyLinkedListNode newNode = new SinglyLinkedListNode(data);
                    return newNode;
                }
                else 
                {                    
                    //get header element step....
                    //because of the method invocation contains llist.head in the first argument
                    //this LLIST will always points to the header node.
                    SinglyLinkedListNode startNode = llist;
                    

                    if (position == 0)
                    {
                        SinglyLinkedListNode newNode = new SinglyLinkedListNode(data);
                        newNode.next = startNode;
                        return newNode;
                    }
                    else
                    {
                        //hopping thruugh nodes to reach position
                        while (position > 1)
                        {
                            llist = llist.next;
                            position--;
                        }
                        //Insert operation to position
                        SinglyLinkedListNode newNode = new SinglyLinkedListNode(data);
                        newNode.next = llist.next;
                        llist.next = newNode;
                        return startNode;

                    }
                    
                }
                
            }

        }

       

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            SinglyLinkedList llist = new SinglyLinkedList();

            int llistCount = Convert.ToInt32(Console.ReadLine());

            for (int i = 0; i < llistCount; i++)
            {
                int llistItem = Convert.ToInt32(Console.ReadLine());
                llist.InsertNode(llistItem);
            }
            SinglyLinkedList.PrintSinglyLinkedList(llist.head, " ");
            Console.WriteLine();

            int data = Convert.ToInt32(Console.ReadLine());

            int position = Convert.ToInt32(Console.ReadLine());

            SinglyLinkedListNode llist_head = llist.InsertNodeAtPosition(llist.head, data, position);

            SinglyLinkedList.PrintSinglyLinkedList(llist_head, " ");
            Console.WriteLine();

        }
    }

}
