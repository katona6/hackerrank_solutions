﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProblemSolving.SparseArrays
{
    using System.CodeDom.Compiler;
    using System.Collections.Generic;
    using System.Collections;
    using System.ComponentModel;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.Serialization;
    using System.Text.RegularExpressions;
    using System.Text;
    using System;

    class Result
    {

        /*
         * Complete the 'matchingStrings' function below.
         *
         * The function is expected to return an INTEGER_ARRAY.
         * The function accepts following parameters:
         *  1. STRING_ARRAY stringList
         *  2. STRING_ARRAY queries
         */

        public static List<int> matchingStrings(List<string> stringList, List<string> queries)
        {
            List<int> result = new();
            int counter = 0;
            foreach (var queryItem in queries)
            {
                counter = 0;
                foreach (var stringListItem in stringList)
                {
                    
                    if (stringListItem.Equals(queryItem))
                    {
                        counter++;
                    }
                    
                }
                result.Add(counter);
            }
            return result;
        }

    }

    class Solution
    {
        public static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            List<string> stringList = new List<string>{ "aba", "baba", "aba", "xzxb" };

            List<string> queries = new List<string> { "aba", "xzxb", "ab" };


            List<int> res = Result.matchingStrings(stringList, queries);

            Console.WriteLine(String.Join("\n", res));

            //textWriter.Flush();
            //textWriter.Close();
        }
    }

}
